﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMovement : MonoBehaviour {

	public float speed; // Create a public variable to control the speed the sprite moves.  Editable in Editor.
	private Vector3 currentDirection; // Create a private direction vector that will hold the current direction the spaceship is facing.
	private float angleToRotate; // Create a float to store the angle we will rotate our ship by depending on the direction the user presses.

	// Use this for initialization
	void Start () {
		// Set the current direction of the spaceship to (0,1,0)
		currentDirection = Vector3.up;
	}
	
	// Update is called once per frame
	void Update () {
		// First we check to see if we are in a situation where we only want to move 1 unit at a time
		if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift)) {
			// If we get here we know that either left shift or right shift is held so we check for our directional presses

			// Check W and Up Arrow to move in the positive Y direction
			if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)){
				transform.rotation = Quaternion.identity; // Set our rotation to identity so its easier to know how to rotate
				transform.position += Vector3.up; // Move our spaceship in the up direction by 1 unit
				currentDirection = Vector3.up; // Set our current direction to where we are facing
			}
			// Check A and Left Arrow to move in the negative X direction
			else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)){
				transform.rotation = Quaternion.identity; // Set our rotation to identity so its easier to know how to rotate
				transform.Rotate (Vector3.forward * 90); // Rotate our transform around the z-axis to look left
				transform.position += Vector3.left;// Move our spaceship in the up direction by 1 unit
				currentDirection = Vector3.left; // Set our current direction to where we are facing
			}
			// Check S and Down Arrow to move in the negative Y direction
			else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)){
				transform.rotation = Quaternion.identity; // Set our rotation to identity so its easier to know how to rotate
				transform.Rotate (Vector3.forward * 180); // Rotate our transform around the z-axis to look down
				transform.position += Vector3.down;// Move our spaceship in the up direction by 1 unit
				currentDirection = Vector3.down; // Set our current direction to where we are facing
			}
			// Check D and Right Arrow to move in the positive X direction
			else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)){
				transform.rotation = Quaternion.identity; // Set our rotation to identity so its easier to know how to rotate
				transform.Rotate (Vector3.forward * -90); // Rotate our transform around the z-axis to look right
				transform.position += Vector3.right;// Move our spaceship in the up direction by 1 unit
				currentDirection = Vector3.right; // Set our current direction to where we are facing
			}

		// If we are not holding shift down we want to do normal movement logic
		} else {
			// Calculate our new direction vector using WASD and normalize.
			Vector3 direction = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0f).normalized;
			// Every frame we add our direction * our speed to our current location.
			transform.position += (direction * speed);
			// Every frame we set our rotation based on which way we are moving.
			// Do nothing if we are not moving or if our direction has not changed
			if (direction != Vector3.zero && currentDirection != direction) {
				// Get the angle between where we were going last frame and where our input is now pointing us
				angleToRotate = Vector3.SignedAngle (currentDirection, direction, Vector3.forward);
				// rotate our sprites transform around the z axis (in/out of the screen) by the angle we just calculated
				transform.RotateAround (transform.position, Vector3.forward, angleToRotate); 
				// make sure to store our current direction for the next frame
				currentDirection = direction;
			}
		}

		// If we have tapped space bar reset our location to the origin
		if (Input.GetKeyDown(KeyCode.Space)){
			transform.position = Vector3.zero;
		}
	}
}
