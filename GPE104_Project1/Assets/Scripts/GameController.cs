﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject goSpaceShip; // variable to store a reference to our spaceship game object.  Set in Editor
	public SpriteMovement smSpaceShip; // variable to store a reference to our sprite mover component. Set in Editor
	public SpriteRenderer srSpaceShip; // variable to store a reference to our sprite renderer component. Set in Editor
	public Sprite[] sprites; // variable to hold an array of sprites to use

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		// Check for the "P" key to disable/enable the movement component of our spaceship
		if (Input.GetKeyDown (KeyCode.P)) {
			if (smSpaceShip != null)
				smSpaceShip.enabled = !smSpaceShip.enabled;
			else
				// log an error if the spritemovement is not properly assigned
				Debug.Log ("SpriteMovement component not found!");
		}
		// Check for the "Q" key to disable/enable our spaceship gameobject
		if (Input.GetKeyDown (KeyCode.Q)){
			if (goSpaceShip != null)
				goSpaceShip.SetActive(!goSpaceShip.activeInHierarchy);
			else
				// log an error if the game object is not properly assigned
				Debug.Log ("SpaceShip GameObject not found!");
		}
		// Check for the "T" key to toggle spaceship color
		if (Input.GetKeyDown (KeyCode.T)) {
			if (srSpaceShip != null)
				// assign a random color to the spaceship. This can result in the same color!
				srSpaceShip.sprite = sprites[Random.Range (0, sprites.Length-1)];
			else
				// log an error if the sprite renderer is not properly assigned
				Debug.Log ("SpriteRenderer component not found!");
		}
		// Check for the "Esc" key to exit our application
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
